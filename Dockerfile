# Используем базовый образ с Python и Debian Bullseye
FROM python:3.7-bullseye

# Установка необходимых инструментов и библиотек
RUN apt-get update && apt-get install -y \
    libaio1 \
    libnsl-dev \
    curl \
    alien \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Создание каталога для Oracle Instant Client
RUN mkdir -p /opt/oracle
COPY . /opt/oracle
RUN alien -i --scripts /opt/oracle/oracle-instantclient19.23-basic-19.23.0.0.0-1.x86_64.rpm

# Настройка переменных окружения
ENV LD_LIBRARY_PATH=/opt/oracle/instantclient_19_10:$LD_LIBRARY_PATH
ENV ORACLE_HOME=/opt/oracle/instantclient_19_10


